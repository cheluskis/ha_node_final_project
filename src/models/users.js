const mongoose = require('mongoose');
const validator = require('validator');

const UserSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            unique: true
        },
        email: {
            type: String,
            required: true,
            validate: validator.isEmail,
        },
        passwordHash: {
            type: String,
            required: true
        },
        hasSession: {
            type: Boolean,
            default: true
        }
        // tweets virtuals de mongoose
    },
    { timestamps: { createdAt: 'created_at' } }
);

const User = mongoose.model('User', UserSchema);

module.exports = { User };
