const mongoose = require('mongoose');
// const validator = require('validator');

const ObjectId = mongoose.Schema.Types.ObjectId;

const TweetSchema = new mongoose.Schema(
    {
        text: {
            type: String,
            required: true,
            unique: true,
            // validate: str => validator.isISO31661Alpha2(str),
        },
        author: {
            type: ObjectId,
            required: true,
            ref: 'Tweet'
        },
        likes: {
            type: Number,
            default: 0,
        }
    },
    { timestamps: { createdAt: 'created_at' } }
);

const Tweet = mongoose.model('Tweet', TweetSchema);

module.exports = { Tweet };
