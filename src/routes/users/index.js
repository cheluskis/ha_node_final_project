const express = require('express');
const checkJwt = require('express-jwt');

const create = require('./create');
const update = require('./update');
const remove = require('./delete');
const { readUser, readUsers } = require('./read');
const checkLoggedIn = require('../helpers/session_helper');


const secret = 'milanesas2018';

module.exports = (app) => {
    const jsonParser = express.json();

    app.post('/users', jsonParser, create);
    app.put('/users/:id', jsonParser, checkJwt({ secret }), checkLoggedIn, update);
    app.delete('/users/:id', jsonParser, checkJwt({ secret }), checkLoggedIn, remove);
    app.get('/users/:id', readUser);
    app.get('/users', jsonParser, readUsers);
};
