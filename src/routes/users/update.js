const { User } = require('../../models/users');

module.exports = async (req, res) => {

    try {
        const user = await User.findByIdAndUpdate(req.params.id,
            req.body
        ).setOptions({ new: true });

        if (user) {
            res.json(user);
        } else {
            res.status(404).json({ error: 'User no encontrado' });
        }
    } catch (error) {
        res.status(500).send({ error });
    }
};
