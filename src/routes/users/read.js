const { User } = require('../../models/users');

module.exports = {

    readUser: async (req, res) => {
        try {
            const user = await User.findById(req.params.id);

            if (user) {
                res.json(user);
            } else {
                res.status(404).json({ error: 'User no encontrado' });
            }

        } catch (error) {
            res.status(500).send({ error });
        }
    },
    readUsers: async (req, res) => {
        const { sortBy, skip } = req.query;
        const order = req.query.order == -1 ? -1 : 1;
        const opts = {};
        opts.sort = sortBy ? { [sortBy]: order } : {};
        opts.skip = parseInt(skip, 10) || 0;
        User.find()
            .setOptions(opts)
            .then(users => res.json(users))
            .catch(error =>
                res.status(500).send({ error, })
            );
    }

};
