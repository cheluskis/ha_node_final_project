const bcrypt = require('bcrypt');

const saltRounds = 10;

const createToken = require('../helpers/jwt_handler');

const { User } = require('../../models/users');

module.exports = async (req, res) => {

    try {
        const { username, email, password } = req.body;
        const hash = await bcrypt.hash(password, saltRounds);
        console.log('hash', hash);

        const user = new User({ username, email, hash });
        const userSaved = await user.save();

        console.log('userSaved', userSaved);
        const token = createToken({ id: userSaved._id, email: userSaved.email });

        console.log('tokenCreado', token);
        return res.json({ token, user: { _id: userSaved._id, email: userSaved.email } });

    } catch (error) {
        return res.json(error);
    }
};
