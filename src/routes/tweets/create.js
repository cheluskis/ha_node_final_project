const { Tweet } = require('../../models/tweets');

module.exports = (req, res) => {
    const tweet = new Tweet(req.body);
    return tweet.save().then(tt => res.json(tt)).catch((error) => res.status(500).send({ error }));
};
