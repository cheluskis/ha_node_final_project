const checkJwt = require('express-jwt');

const secret = 'milanesas2018';


const express = require('express');
const create = require('./create');
const update = require('./update');
const remove = require('./delete');
const { readTweet, readTweets } = require('./read');
const checkLoggedIn = require('../helpers/session_helper');


module.exports = (app) => {
    const jsonParser = express.json();

    app.post('/tweets', checkJwt({ secret }), checkLoggedIn, jsonParser, create);
    app.put('/tweets/:id', checkJwt({ secret }), checkLoggedIn, jsonParser, update);
    app.delete('/tweets/:id', checkJwt({ secret }), checkLoggedIn, remove);
    app.get('/tweets/:id', readTweet);
    app.get('/tweets', readTweets);

};
