const { Tweet } = require('../../models/tweets');

module.exports = {

    readTweet: async (req, res) => {
        try {
            const tweet = await Tweet.findById(req.params.id).populate('author');

            if (tweet) {
                res.json(tweet);
            } else {
                res.status(404).json({ error: 'Tweet no encontrado' });
            }

        } catch (error) {
            res.status(500).send({ error });
        }
    },
    readTweets: (req, res) => {
        const { sortBy, skip, author } = req.query;
        const order = req.query.order == -1 ? -1 : 1;
        const opts = {};
        opts.sort = sortBy ? { [sortBy]: order } : {};
        opts.skip = parseInt(skip, 10) || 0;
        let query = {};
        if (author) {
            query = {author};
        }

        Tweet.find(query)
            .populate('author')
            .setOptions(opts)
            .then(tweets => res.json(tweets))
            .catch(error =>
                res.status(500).send({ error, })
            );
    }

};
