const { Tweet } = require('../../models/tweets');

module.exports = async (req, res) => {
    try {
        const tweet = await Tweet.findByIdAndRemove(req.params.id);

        if (tweet) {
            res.json(tweet);
        } else {
            res.status(404).json({ error: 'Tweet no encontrado' });
        }
    } catch (error) {
        res.status(500).send({ error });
    }
};
