const User = require('../../models/users');

module.exports = async (req, res, next) => {
    try {
        const user = await User.findById(req.user.id);
        if (user) {
            if (!user.hasSession) {
                res.status(401).json({ error: 'Unauthenticated User' });
            } else {
                next();
            }
        } else {
            res.status(401).json({ error: 'Unauthorized User' });
        }
    } catch (e) {
        res.status(500).json({ error: e });
    }
};
