const checkJwt = require('express-jwt');
const express = require('express');
const bcrypt = require('bcrypt');

const User = require('../../models/users');
const createToken = require('../helpers/jwt_handler');
const checkLoggedIn = require('../helpers/session_helper');

const secret = 'milanesas2018';

module.exports = (app) => {
    const jsonParser = express.json();
    app.post('/sessions', jsonParser, async (req, res) => {
        // login
        try {
            const user = await User.findOne({
                $or: [{ email: req.body.email }, { username: req.body.username }]
            });
            if (!user) {
                res.json({ error: 'user does not exist' });
            }
            console.log('userIS', user);
            const match = await bcrypt.compare(req.body.password, user.hash);
            if (!match) {
                res.json({ error: 'user does not exist' });
            }
            const token = createToken({ id: user._id, username: user.username, email: user.email });
            return res.json(token);
        } catch (error) {
            return res.json(error);
        }

    });

    app.get('/sessions', checkJwt({ secret }), checkLoggedIn, jsonParser, async (req, res) => {
        // new refreshed token
        try {
            return createToken({ id: req.user.id, username: req.user.username, email: req.user.email });
        } catch (error) {
            return res.json(error);
        }
    });

    app.delete('/sessions', checkJwt({ secret }), checkLoggedIn, jsonParser, async (req, res) => {
        // logout
        try {
            const user = await User.findById(req.user.id);
            user.hasSession = false;
            user.save();
            return res.json('loggedout');
        } catch (error) {
            return res.json(error);
        }
    });

};
