const express = require('express');
const usersRoutes = require('./routes/users');
const tweetsRoutes = require('./routes/tweets');


const app = express();
usersRoutes(app);
tweetsRoutes(app);


app.listen(3000, () => console.log('Servidor corriendo en el puerto 3000!'));
